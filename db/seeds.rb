# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
topics, users = [], []

3.times {|time| topics << Topic.create(title: "Topic #{time}")}
3.times {|time| users << User.create(name: "User #{time+1}", topics: topics)}

topics.each do |topic|
  3.times { |time| users.first.messages.create(content: "Message #{time}", topic: topic)}
end

