class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :content
      t.belongs_to :user, index: true
      t.belongs_to :topic, index: true

      t.timestamps null: false
    end
    add_foreign_key :messages, :users
    add_foreign_key :messages, :topics
  end
end
