class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :content
      t.belongs_to :user, index: true
      t.belongs_to :message, index: true

      t.timestamps null: false
    end
    add_foreign_key :notifications, :topics
    add_foreign_key :notifications, :messages
  end
end
