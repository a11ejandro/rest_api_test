require "rails_helper"

describe "Notifications API" do
  before do
    @user = create :user
    @poster = create :user
    @topic = create(:topic, users: [@user])
    @message = create(:message, user: @poster, topic: @topic)
  end

  it 'should be created for subscribed users' do
    get_json api_user_path(@user)
    expect(json['notifications'].count).to eq 1
  end

  it 'should not be sent to author of message' do
    get_json api_user_path(@poster)
    expect(json['notifications']).to be_empty
  end
end

