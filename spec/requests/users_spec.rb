require "rails_helper"

describe "Users API" do
  it 'displays list of all users' do
    @users = create_list(:user, 3)

    get_json api_users_path

    expect(json.count).to eq 3
  end

  it 'displays all topics and notifications on request' do
    @user = create(:user)
    @topic1 = create(:topic, users: [@user])
    @topic2 = create(:topic, users: [@user])
    @poster = create(:user)
    @message = create(:message, user: @poster, topic: @topic1)

    get_json api_user_path(@user)
    expect(json['topics'].count).to eq 2
    expect(json['notifications'].count).to eq 1
  end

  it 'can subscribe to topic' do
    @user = create(:user)
    @topic = create(:topic)

    post_json subscribe_api_user_path(@user), {topic_id: @topic.id}
    expect(json['topics'].first['title']).to eq @topic.title
  end

  it 'can unsubscribe from topic' do
    @user = create(:user)
    @topic = create(:topic, users: [@user])

    post_json unsubscribe_api_user_path(@user), {topic_id: @topic.id}
    expect(json['topics']).to be_empty
  end
end