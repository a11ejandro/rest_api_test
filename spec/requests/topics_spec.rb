require "rails_helper"

describe 'Topics API' do
  it 'gives a list of all topics' do

    @topics = create_list(:topic, 5)

    get_json api_topics_path

    expect(response).to be_success
    expect(json.count).to eq 5
    expect(json.first['title']).to eq @topics.first.title
  end

  it 'gives a single topic with messages' do
    @user = create(:user)
    @topic = create :topic, title: 'Topic 1'
    @messages = create_list(:message, 5, topic: @topic, user: @user)

    get_json api_topic_path(@topic)

    expect(response).to be_success
    expect(json['title']).to eq 'Topic 1'
    expect(json['messages'].count).to eq 5
  end
end