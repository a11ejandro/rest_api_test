require "rails_helper"

describe "Messages API" do
  before do
    @topic = create(:topic)
    @user = create(:user)
  end

  it 'displays a single message' do
    @message = create(:message, user: @user, topic: @topic)
    get_json api_message_path(@message)

    expect(response).to be_success
    expect(json['content']).to eq @message.content
  end

  it 'allows to create a new message' do
    post_json api_messages_path, { message: {content: 'Test message', user_id: @user.id, topic_id: @topic.id} }
    expect(json['content']).to eq 'Test message'
  end
end