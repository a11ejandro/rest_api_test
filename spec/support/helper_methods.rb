def json
  @json ||= JSON.parse(response.body)
end

def get_json(path)
  get path, {}, { "Accept" => "application/json" }
end

def post_json(path, params={})
  post path, params, { "Accept" => "application/json" }
end
