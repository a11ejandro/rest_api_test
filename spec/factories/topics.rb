FactoryGirl.define do
  factory :topic do
    title { Faker::Lorem.word }
  end
end