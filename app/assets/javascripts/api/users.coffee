# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

client = new Faye.Client('/faye')

client.subscribe '/api/users/2/notifications', (payload)->
  $('#notifications').append("<li>#{payload['data']}</li>")