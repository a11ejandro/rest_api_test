class Notification < ActiveRecord::Base
  belongs_to :message
  belongs_to :user

  before_create :set_content

  validates_presence_of :message, :user

  private
  def set_content
    self.content = "#{message.user.name} sent message to #{message.topic.title}"
  end
end
