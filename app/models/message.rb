class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic
  has_many :notifications

  after_create :create_notifications

  validates_length_of :content, in: 1..255
  validates_presence_of :user,:topic

  private
  def create_notifications
    self.topic.users.each {|u| u.notify_about_message(self) unless u === self.user}
  end
end
