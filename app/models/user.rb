class User < ActiveRecord::Base

  has_and_belongs_to_many :topics
  has_many :messages
  has_many :notifications

  validates_presence_of :name

  def notify_about_message(message)
    self.notifications.create(message: message)
  end

  def subscribe_to(topic)
    unless topics.include? topic
      topics << topic
      self.save
    end
  end

  def unsubscribe_from(topic)
    if topics.include? topic
      topics.delete topic
      self.save
    end
  end

  def to_json(options)
    super({only: [:id, :name], include: [:notifications, :topics]}.merge(options))
  end

end
