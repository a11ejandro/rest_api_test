class RealtimeNotificationsController < FayeRails::Controller
  require 'eventmachine'

  observe Notification, :after_create do |new_notification|
    # Prevent errors from not running EM.
    Thread.new { EventMachine.run } unless EventMachine.reactor_running? && EventMachine.reactor_thread.alive?

    # Use native Faye methods because FayeRails::Controller.publish doesn't work properly.
    client = Faye::Client.new('http://localhost:3000/faye')
    client.publish("/api/users/#{new_notification.user_id}/notifications", data: new_notification.content)
  end
end