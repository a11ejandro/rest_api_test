class Api::MessagesController < ApplicationController
  before_action :set_message, only: :show

  def create
    @message = Message.new(message_params)
    if @message.save
      render json: @message
    else
      render json: {error: "An error occured"}
    end
  end

  def show
    render json: @message
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:content, :user_id, :topic_id)
  end
end
