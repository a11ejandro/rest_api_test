class Api::TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :subscribe, :unsubscribe]
  before_action :set_user, only: [:subscribe, :unsubscribe]

  def index
    @topics = Topic.all
    respond_to do |format|
      format.json {render json: @topics}
    end
  end

  def show
    respond_to do |format|
      format.json {render json: @topic.to_json(include: :messages)}
    end
  end

  private
  def set_topic
    @topic = Topic.find(params[:id])
  end

  def set_user
    @user = User.find(params[:id])
  end
end
