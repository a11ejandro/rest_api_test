class Api::UsersController < ApplicationController
  before_action :set_user, only: [:show, :subscribe, :unsubscribe]
  before_action :set_topic, only: [:subscribe, :unsubscribe]

  def index
    @users = User.all
    render json: @users
  end

  def show
    respond_to do |format|
      format.json { render json: @user }
      format.html
    end
  end

  def subscribe
    @user.subscribe_to @topic
    render json: @user
  end

  def unsubscribe
    @user.unsubscribe_from @topic
    render json: @user
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def set_topic
    @topic = Topic.find(params[:topic_id])
  end
end
